using UnrealBuildTool;

public class BackroomsDigitalArtEditorTarget : TargetRules
{
	public BackroomsDigitalArtEditorTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V3;
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		Type = TargetType.Editor;
		ExtraModuleNames.Add("BackroomsDigitalArt");
	}
}
