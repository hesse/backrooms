using UnrealBuildTool;

public class BackroomsDigitalArt : ModuleRules
{
	public BackroomsDigitalArt(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateDependencyModuleNames.Add("Core");
		PrivateDependencyModuleNames.Add("Core");
	}
}
