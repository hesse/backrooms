using UnrealBuildTool;

public class BackroomsDigitalArtServerTarget : TargetRules
{
	public BackroomsDigitalArtServerTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V3;
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		Type = TargetType.Server;
		ExtraModuleNames.Add("BackroomsDigitalArt");
	}
}
