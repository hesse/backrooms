using UnrealBuildTool;

public class BackroomsDigitalArtTarget : TargetRules
{
	public BackroomsDigitalArtTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V3;
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		Type = TargetType.Game;
		ExtraModuleNames.Add("BackroomsDigitalArt");
	}
}
